#+TITLE: Visualisation de données avec Seaborn : Travaux Pratiques
#+AUTHOR: Bernard Hugueney
#+DATE: <2020-12-07 Mon 16:00>
#+LANGUAGE:  fr



#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


On voudra visualiser les informations apportées par le jeu de données
sur les diamants qui est mis à disposition comme jeu d'exemples par la
bibliothèque Seaborn. On peut le charger comme suit :

#+BEGIN_SRC ipython
%matplotlib inline
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
diamonds = sns.load_dataset("diamonds")
#+END_SRC

*Remarque :* Si le chargement échoue lors d'une exécution en local à
cause de restrictions de sécurité réseau qui exigent l'utilisation
d'un proxy, on peut télécharger [[https://raw.githubusercontent.com/mwaskom/seaborn-data/master/diamonds.csv][le fichier]] à l'aide du navigateur et
utiliser ensuite directement ce fichier à la place.


* Informations générales

Commencer par afficher les noms des colonnes, puis les types des
colonnes et la taille en mémoire de la ~DataFrame~.

#+BEGIN_SRC ipython
# À faire !
#+END_SRC


* Valeurs manquantes ou aberrantes

** Localisation

Chercher s'il y a des valeurs manquantes ou aberrantes (par exemple des dimensions à 0).

#+BEGIN_SRC ipython
# À faire !
#+END_SRC


** Corrections

Est-ce qu'il est préférable de remplacer les valeurs aberrantes ou de
supprimer les lignes ou colonnes contenant des valeurs aberrantes ?

Supprimer les lignes avec valeurs aberrantes.


#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Vérifier qu'il n'y a plus de valeurs aberrantes :


#+BEGIN_SRC ipython
# À faire !
#+END_SRC

* Tailles des diamants en carats

Afficher les moyenne, déviations standard, et quartiles de la taille
en carats des diamants :


#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Afficher l'histogramme de la taille des diamants en carats :


#+BEGIN_SRC ipython
# À faire !
#+END_SRC


Utiliser 100 subdivisions (/bins/) de la taille en carats pour
afficher l'histogramme.

#+BEGIN_SRC ipython
# À faire !
#+END_SRC


Afficher la distribution de la /table/ des diamants :


#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Afficher la distribution du prix des diamants :


#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Afficher les distributions des dimension ~x~, ~y~ et ~z~.

Représenter graphiquement les nombre de diamants dans chaque catégorie
de qualité de taille (~cut~): 'Ideal','Premium','Very Good', 'Good',
'Fair' :


#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Représenter graphiquement le nombre de diamants dans chaque catégorie
de couleur :


#+BEGIN_SRC ipython
# À faire !
#+END_SRC


Représenter graphiquement le nombre de diamants dans chaque catégorie
de clareté (/clarity/): 'F','IF', 'WS1','WS2','VS1','VS2', 'SI1','SI2',
'I1', 'I2', 'I3'

#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Pour chacune des catégorie de clareté, représenter graphiquement le
rapport entre la taille en carats et le prix :


#+BEGIN_SRC ipython
# À faire !
#+END_SRC


Représenter graphiquement la relation entre le prix et la dimension
~x~ :

#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Représenter graphiquement la relation entre la taille en carats et la
dimension ~x~ :

#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Représenter graphiquement les distributions de prix pour chaque
catégorie de taille :

#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Représenter graphiquement les relations entre toutes les paires
d'attributs :

#+BEGIN_SRC ipython
# À faire !
#+END_SRC

Représenter graphiquement les corrélations entre toutes les paires
d'attributs :

#+BEGIN_SRC ipython
# À faire !
#+END_SRC
