g = sns.lmplot(x="table",y="price", data=diamonds)
model = linear_model.LinearRegression()
results = model.fit(diamonds[["depth", "table"]], diamonds['price'])
print("The best linear modeling is price= %f + %f×depth + %f×table" %(results.intercept_, results.coef_[0], results.coef_[1]))
