from sklearn.preprocessing import StandardScaler
scaler=StandardScaler().fit(X_train)
grid_search_knn_scaled = GridSearchCV(
    estimator= KNeighborsClassifier(),
    param_grid={'n_neighbors': range(1,10),
               'weights': ('uniform','distance')},
    scoring='precision',
    cv=n_fold,
    refit=True,
    verbose=True
, n_jobs=-1)# autant de tâches en parallèle que possible (que de coeurs)
grid_search_knn_scaled.fit(scaler.transform(X_train), y_train)
conf_mat_grid_search_knn_scaled = confusion_matrix(y_test, grid_search_knn_scaled.predict(scaler.transform(X_test)))
print(conf_mat_grid_search_knn_scaled)
sns.heatmap(conf_mat_grid_search_knn_scaled,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs sur le jeu de test\npar K plus proches voisins\nsur données normalisées\nhyperparamètres optimisés par grid search");
