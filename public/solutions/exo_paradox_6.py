import seaborn as sns
from scipy.stats.stats import pearsonr
basketball= pd.read_csv("../../data/Berkson-Paradox/players_stats.csv")
basketball=basketball.loc[:,['Height','PTS']].dropna()
g=sns.jointplot(x='Height', y='PTS', data= basketball, kind="reg")
r,p_value=pearsonr(basketball['PTS'],basketball['Height'])
g.fig.suptitle(f"pearson R={r:.4E}, p value={p_value:.4E}");
