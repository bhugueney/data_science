%%time
import xgboost as xgb

param_grid = {
        'silent': [False],
        'max_depth': [6, 10, 15, 20],
        'learning_rate': [0.001, 0.01, 0.1, 0.2, 0,3],
        'subsample': [0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'colsample_bytree': [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'colsample_bylevel': [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'min_child_weight': [0.5, 1.0, 3.0, 5.0, 7.0, 10.0],
        'gamma': [0, 0.25, 0.5, 1.0],
        'reg_lambda': [0.1, 1.0, 5.0, 10.0, 50.0, 100.0],
        'n_estimators': [100]}

xgb_search = RandomizedSearchCV(xgb.XGBClassifier(), param_grid, n_iter=n_iter,
                            n_jobs=-1, verbose=3, cv=n_fold,
                            scoring='precision', refit=True)
xgb_search.fit(scaler.transform(X_train), y_train)
conf_mat_xgb_search = confusion_matrix(y_test,xgb_search.predict(scaler.transform(X_test)))
print(conf_mat_xgb_search)
sns.heatmap(conf_mat_xgb_search,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs sur le jeu de test\npar XGBoost\nsur données normalisées\nhyperparamètres optimisés par grid search");
