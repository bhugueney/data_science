data=pd.get_dummies(diamonds, columns=['clarity','cut'], drop_first=True)
data['carat²']=data['carat']*data['carat']
data['carat³']=data['carat²']*data['carat']
data
