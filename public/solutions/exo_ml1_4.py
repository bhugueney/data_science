decision_tree = DecisionTreeClassifier().fit(X_train, y_train)
conf_mat_decision_tree = confusion_matrix(y_test, decision_tree.predict(X_test))
print(conf_mat_decision_tree)
sns.heatmap(conf_mat_decision_tree,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs sur le jeu de test\npar arbre de décision");
