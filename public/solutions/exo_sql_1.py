import pandas as pd
import sqlite3
conn = sqlite3.connect("Data/Databases/ccs.sqlite")

countries = pd.read_sql_query("SELECT * FROM countries;", conn)

# Be sure to close the connection
conn.close()
countries
