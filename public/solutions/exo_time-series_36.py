sns.set(font_scale=5)
g=sns.catplot(y='t', x='year', kind='violin',col='month',col_wrap=3,data=local_temps, height=16)
plt.title("Températures (°K)")
for ax in g.axes:
    for label in ax.get_xticklabels():
        label.set_fontsize(30)
        label.set_rotation(45)
