g = sns.lmplot(x="carat", y="price", data=diamonds)
from sklearn import linear_model
model = linear_model.LinearRegression()
results = model.fit(diamonds[["carat"]], diamonds['price'])
print("The best linear modeling is price= %f + %f×carat" %(results.intercept_, results.coef_))
