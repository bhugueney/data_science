from statsmodels.graphics.tsaplots import plot_acf, plot_pacf
plot_acf(df['Global_active_power'], lags=24*7*2);
plot_pacf(df['Global_active_power'], lags=24*7*2);
