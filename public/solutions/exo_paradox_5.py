florida= pd.read_csv("./Data/Simpson-Paradox/Florida-death-penalty-victim.csv")
tmp=florida.groupby(['Victim','Defendent','Death']).count().reset_index(2).pivot(columns='Death')
(tmp.iloc[:,1]*100/(tmp.iloc[:,0]+tmp.iloc[:,1])).rename("% death penalty")
