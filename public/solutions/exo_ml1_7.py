grid_search_knn = GridSearchCV(
    estimator= KNeighborsClassifier(),
    param_grid={'n_neighbors': range(1,10),
               'weights': ('uniform','distance')},
    scoring='precision',
    cv=n_fold,
    refit=True,
    verbose=True
, n_jobs=-1)# autant de tâches en parallèle que possible (que de coeurs)
grid_search_knn.fit(X_train, y_train)
conf_mat_grid_search_knn = confusion_matrix(y_test, grid_search_knn.predict(X_test))
print(conf_mat_grid_search_knn)
sns.heatmap(conf_mat_grid_search_knn,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs sur le jeu de test\npar K plus proches voisins\nhyperparamètres optimisés par grid search");
