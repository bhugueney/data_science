florida= pd.read_csv("./Data/Simpson-Paradox/Florida-death-penalty.csv")
tmp=florida.groupby(['Defendent','Death']).count().reset_index(1).pivot(columns=['Death'])
(tmp.iloc[:,1]*100/(tmp.iloc[:,0]+tmp.iloc[:,1])).rename("% death penalty")
