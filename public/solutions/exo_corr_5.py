from scipy.stats.stats import pearsonr
g = sns.JointGrid(x="carat", y="price", data=diamonds)
g = g.plot_joint(plt.scatter, color="g", s=40, edgecolor="white")
g = g.plot_marginals(sns.histplot, kde=False, color="g")
r,p_value=pearsonr(diamonds['carat'],diamonds['price'])
g.ax_joint.text(3,2500,f"pearson R={r:.4E}\np value={p_value:.4E}");
