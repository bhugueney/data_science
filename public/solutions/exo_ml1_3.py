from sklearn.model_selection import train_test_split
X_train,X_test,y_train,y_test = train_test_split(X, y,test_size=0.25)
lr_train= LogisticRegression(max_iter= 10000)
lr_train.fit(X_train, y_train)
conf_mat_test = confusion_matrix(y_test, lr_train.predict(X_test))
print(conf_mat_test)
sns.heatmap(conf_mat_test,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs sur le jeu de test\npar régression logistique");
