import pandas as pd
berkeley= pd.read_csv("./Data/Simpson-Paradox/UC-Berkeley-applicants-no-dept.csv")
groups= berkeley.groupby(['Gender','Admit']).count()
w_acc=groups.loc['Female','Admitted']/berkeley.groupby('Gender').count().loc['Female']['Id']
m_acc=groups.loc['Male','Admitted']/berkeley.groupby('Gender').count().loc['Male']['Id']
(w_acc,m_acc)
