fig, axs = plt.subplots(nrows=3)
fig.set_size_inches(16,11)
sns.distplot(diamonds["x"], ax=axs[0])
sns.distplot(diamonds["y"], ax=axs[1])
sns.distplot(diamonds["z"], ax=axs[2]);
