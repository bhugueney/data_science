from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import RandomizedSearchCV
import numpy as np
# Number of trees in random forest
n_estimators = [int(x) for x in np.linspace(start = 200, stop = 2000, num = 10)]
# Number of features to consider at every split
max_features = ['auto', 'sqrt']
# Maximum number of levels in tree
max_depth = [int(x) for x in np.linspace(10, 110, num = 11)]
max_depth.append(None)
# Minimum number of samples required to split a node
min_samples_split = [2, 5, 10]
# Minimum number of samples required at each leaf node
min_samples_leaf = [1, 2, 4]
# Method of selecting samples for training each tree
bootstrap = [True, False]# Create the random grid
rf_search = GridSearchCV(
    estimator= RandomForestClassifier(),
    param_grid={'n_estimators' : n_estimators,
            'max_features' : max_features,
            'max_depth' : max_depth,
            'min_samples_split':min_samples_split,
            'min_samples_leaf': min_samples_leaf,
            'bootstrap' : bootstrap},
    scoring='precision',
    cv=n_fold,
    refit=True,
    verbose=True
, n_jobs=-1)# autant de tâches en parallèle que possible (que de coeurs)
rf_search.fit(scaler.transform(X_train), y_train)
conf_mat_rf_search = confusion_matrix(y_test,rf_search.predict(scaler.transform(X_test)))
print(conf_mat_rf_search)
sns.heatmap(conf_mat_rf_search,annot = True, linewidths=0.5,fmt=".0f",xticklabels=target_labels, yticklabels=target_labels)
plt.xlabel("diagnostic modélisé")
plt.ylabel("diagnostic réel")
plt.title("Diagnostic de tumeurs sur le jeu de test\npar Random Forest\nsur données normalisées\nhyperparamètres optimisés par grid search");
