#+TITLE: Acquisition de données : Travaux Pratiques (fr-FR)
#+AUTHOR: Bernard Hugueney
#+DATE: <2021-06-28 Mon 14:30>
#+LANGUAGE:  fr



#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC

* JSON

** JSON 'plat'

Le code ci-dessous sauvegarde des données au format JSON dans un
fichier.

#+BEGIN_SRC ipython
import json
data=[{"Lang_name":"C", "Author": "Dennis Ritchie", "Year" : 1972},
      {"Lang_name":"Python", "Author":"Guido Van Rossum", "Year":1989},
      {"Lang_name":"Lisp", "Author":"John McCarthy", "Year":1958}]
with open("Data/JSON/Programming_languages.json", "w") as out:
    json.dump(data, out)
#+END_SRC

Charger ces données dans une /DataFrame/.

#+BEGIN_SRC ipython
#%load hints/exo_json_1-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_json_1-1.py
#%load solutions/exo_json_1.py
import pandas as pd
help(pd.read_json)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_json_1.py
import pandas as pd
pd.read_json("Data/JSON/Programming_languages.json")
#+END_SRC


** JSON hiérarchique (/nested/)

   Le code ci-dessous sauvegarde des données au format JSON dans un
fichier.

#+BEGIN_SRC ipython
data=[{"Lang_name":"C", "Author": {"name":"Dennis Ritchie", "year_of_birth": 1941}, "Year" : 1972},
      {"Lang_name":"Python", "Author": {"name":"Guido Van Rossum", "year_of_birth": 1956}, "Year":1989},
      {"Lang_name":"Lisp", "Author": {"name":"John McCarthy", "year_of_birth": 1927}, "Year":1958}]
with open("Data/JSON/Programming_languages-nested.json", "w") as out:
    json.dump(data, out)
#+END_SRC

Charger ces données dans une /DataFrame/.

#+BEGIN_SRC ipython
#%load hints/exo_json_2-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_json_2-1.py
#%load solutions/exo_json_2.py
import pandas as pd
help(pd.json_normalize)
#+END_SRC


#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_json_2.py
df= None
with open("Data/JSON/Programming_languages-nested.json") as input:
    loaded_data = json.load(input)
    df= pd.json_normalize(loaded_data)
df
#+END_SRC


* HTML

Lire les données «COVID-19 cases in France by region and date» de la
page HTML [[https://en.wikipedia.org/wiki/COVID-19_pandemic_in_France]]
(une copie locale est disponible dans le fichier
=./Data/HTML/COVID-19_pandemic_in_France= ).

  
#+BEGIN_SRC ipython
#%load hints/exo_html_1-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_html_1-1.py
#%load solutions/exo_html_1.py
import pandas as pd
help(pd.read_html)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_html_1.py
import pandas as pd
#dfs = pd.read_html("https://en.wikipedia.org/wiki/COVID-19_pandemic_in_France", match='COVID-19 cases in France by region and date')
dfs = pd.read_html("Data/HTML/COVID-19_pandemic_in_France-broken", match='COVID-19 cases in France by region and date')
dfs[0]
#+END_SRC

* SQL


** Basique

Créer une ~DataFrame~ avec les codes sur deux lettres, noms et
préfixes téléphoniques des pays dans la table =countries= de la base
de données /sqlite/ du fichier =./Data/Databases/ccs.sqlite= .

#+BEGIN_SRC ipython
#%load hints/exo_sql_1-1.py
#TODO
#+END_SRC


#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_sql_1-1.py
#%load solutions/exo_sql_1.py
import sqlite3
import pandas as pd
help(pd.read_html)
help(sqlite3.connect)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_sql_1.py
import pandas as pd
import sqlite3
conn = sqlite3.connect("Data/Databases/ccs.sqlite")

countries = pd.read_sql_query("SELECT * FROM countries;", conn)

# Be sure to close the connection
conn.close()
countries
#+END_SRC

** Avancé

Créer une ~DataFrame~ avec les noms de toutes les villes de Franceetle
 nom de leur /Département/ à partir des la base de données /sqlite/ du
 fichier =./Data/Databases/ccs.sqlite=. Les villes sont dans la table
 =cities= et les /Départements/ dans la table =states=.


#+BEGIN_SRC ipython
#%load hints/exo_sql_2-1.py
#TODO
#+END_SRC


#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_sql_2-1.py
#%load solutions/exo_sql_2.py
import sqlite3
import pandas as pd
help(pd.read_html)
help(sqlite3.connect)
# perform a JOIN by SELECTing from multiple tables WHERE XXX_id in one table = id in XXX table
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_sql_2.py
conn = sqlite3.connect("Data/Databases/ccs.sqlite")

french_cities = pd.read_sql_query("SELECT cities.name as name, states.name as state FROM cities, states, countries WHERE cities.state_id = states.id AND states.country_id = countries.id AND countries.name= 'France';", conn)

conn.close()
french_cities
#+END_SRC


** Bonus


Comment trouveriez-vous les noms des tables de la base de données
/sqlite/ contenue dans le fichier =./Data/Databases/ccs.sqlite= ?

#+BEGIN_SRC ipython
#%load solutions/exo_sql_3.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_sql_3.py
import pandas as pd
import sqlite3
conn = sqlite3.connect("Data/Databases/ccs.sqlite")

tables = pd.read_sql_query("SELECT name FROM sqlite_master WHERE type='table';", conn)

# Be sure to close the connection
conn.close()
tables
#+END_SRC
