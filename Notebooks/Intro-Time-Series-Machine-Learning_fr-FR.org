#+TITLE: Visualisation et Prévision de séries temporelles (fr-FR)
#+AUTHOR: Bernard Hugueney
#+DATE: <2021-07-01 Thu 13:30>
#+LANGUAGE:  fr



#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC

* Analyse de séries temporelles
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

1. Chargement
2. Prétraitements
3. Modèles descriptifs & Visualisations
4. Modèles prédictifs


* Extrapolation vs Interpolation
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

- Il est beaucoup plus difficilede prédire le futur que le passé !
  - le découpage apprentissage / validation / test doit être chronologique
- surtout le futur éloigné :
  - (N pas plus loin vs 1 pas plus loin)

* Valeurs manquantes
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Compte-tenu de l'auto-corrélation et de la nécessité d'avoir des
   séries temporelles régulières pour la plupart des algorithmes, il
   est souvent pertinent de faire de l'interpolation ou du /forward
   fill/ pour remplacer les valeurs manquantes.

* Attributs chronologiques
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

Enrichissement des données :
- générique (e.g. jour de la semaine)
- spécifique (e.g. vacances)


* Prévisibilité ?
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:

On peut essayer de prévoir [[https://www.eia.gov/dnav/pet/hist_xls/RBRTEd.xls][toutes sortes de séries temporelles]] mais
les prix de marchés ne sont pas un objectif facile aux échalles de
temps où [[https://fr.wikipedia.org/wiki/Hypoth%C3%A8se_des_march%C3%A9s_financiers_efficients][le marché est efficient]].

* Exemples
   :PROPERTIES:
   :metadata: (slideshow . ((slide_type . slide)))
   :END:



* Analyse de séries temporelles

On va s'intéresser au jeu de données de l'[[https://data.open-power-system-data.org/time_series/latest/][OpenPower System Data]].

On a sélectionné et renommé quelques colonnes qui concernent la France :

#+BEGIN_SRC ipython
import pandas as pd
ops_FR=pd.read_csv("./Data/Time-Series/Open-Power-System-Data_France.csv.zip",parse_dates=[0],index_col=0)
ops_FR.shape
#+END_SRC

On supprime toutes les lignes qui contiennent au moins une valeur manquante :

#+BEGIN_SRC ipython
ops_FR=ops_FR.dropna(how='any')
ops_FR.shape
#+END_SRC

On constate que les dates qui servent d'index et celles de la colonne
date ne sont pas dans le même fuseau horaire. On peut convertir
l'index (en UTC) au fuseau horaire local de Paris :

#+BEGIN_SRC ipython
ops_FR.index = ops_FR.index.tz_convert('Europe/Paris')
ops_FR
#+END_SRC

Afin de pouvoir faire des sélections basées sur des attributs
chronologiques, on ajoute souvent des colonnes avec de tels attributs
calculés à partir de la date :

#+BEGIN_SRC ipython
ops_FR['year'] = ops_FR.index.year
ops_FR['month'] = ops_FR.index.month
ops_FR['weekday'] = ops_FR.index.day_name()
#+END_SRC


#+BEGIN_SRC ipython
ops_FR['dayofweek'] = ops_FR.index.dayofweek
ops_FR['hour'] = ops_FR.index.hour
ops_FR['quarter'] = ops_FR.index.quarter
ops_FR['dayofyear'] = ops_FR.index.dayofyear
ops_FR['dayofmonth'] = ops_FR.index.day
ops_FR['weekofyear'] = ops_FR.index.weekofyear
#+END_SRC

#+BEGIN_SRC ipython
ops_FR.sample(10, random_state=42)
#+END_SRC

#+BEGIN_SRC ipython
ops_FR=ops_FR.fillna(method='ffill')
ops_FR=ops_FR.dropna(how='any')
ops_FR.isnull().sum()
#+END_SRC

#+BEGIN_SRC ipython
ops_FR=ops_FR.asfreq('H', method='ffill')
#+END_SRC


Pour la visualisations, on peut utiliser /Seaborn/ comme pour
n'importe quelle ~DataFrame~ :

#+BEGIN_SRC ipython
%matplotlib inline
import seaborn as sns
sns.lmplot(x="solar", y="wind", data=ops_FR);
#+END_SRC

#+BEGIN_SRC ipython
sns.pairplot(data=ops_FR, hue='year', kind="reg", vars=['load_actual','load_forecast', 'wind','solar']);
#+END_SRC


#+BEGIN_SRC ipython
sns.jointplot(y="solar", x="load_actual", data=ops_FR, kind="kde", color="#4CB391");
#+END_SRC


#+BEGIN_SRC ipython
sns.jointplot(y="solar",ylim=(0,3000), x="load_actual",xlim=(0,90000), data=ops_FR, kind="kde", color="#4CB391");
#+END_SRC


#+BEGIN_SRC ipython
sns.jointplot(y="wind",ylim=(0,6000), x="load_actual",xlim=(0,90000), data=ops_FR, kind="kde", color="#4CB391");
#+END_SRC


On peut aussi utiliser /Pandas/ pour afficher la série temporelle, en
sélectionnant la ou les valeurs à afficher ainsi que l'intervalle de
dates considéré. Il est possible d'utiliser /Matplotlib/ pour modifier
l'affichage de /Pandas/:


#+BEGIN_SRC ipython
sns.set(rc={'figure.figsize':(12, 12)})
ax = ops_FR.loc['2018-01':'2018-02', ['load_forecast','load_actual']].plot()
ax.set_ylabel('Consommation électrique (GWh)')
#+END_SRC


Pour que les dates soient correctement affichées sur l'abscisse, on
doit utiliser ~pd.plotting.register_matplotlib_converters()~ :

#+BEGIN_SRC ipython
pd.plotting.register_matplotlib_converters()
ax = ops_FR.loc['2018-01':'2018-02', 'load_actual'].resample('D').mean().plot()
ax.set_ylabel('Consommation électrique (GWh)')
#+END_SRC

Les attributs chronologiques synthétisés peuvent être utiles pour les
visualisations :

#+BEGIN_SRC ipython
sns.catplot(x='month', y='load_actual', data=ops_FR, kind='violin', height=12, col='year', col_wrap=3);
#+END_SRC


#+BEGIN_SRC ipython
import matplotlib.pyplot as plt
#+END_SRC

#+BEGIN_SRC ipython
sns.pairplot(ops_FR,
             hue='hour',
             x_vars=['hour','weekday','year','weekofyear'],
             y_vars='load_actual',
             height=5,
             plot_kws={'alpha':0.1, 'linewidth':0}
            )
plt.suptitle("Puissance électrique consommée GW par Heure, jour de semaine, année et semaine de l'année");
#+END_SRC

#+BEGIN_SRC ipython
data_columns = ['load_actual', 'load_forecast','wind', 'solar']
ops_FR_daily_mean= ops_FR[data_columns].resample('D').mean()
#+END_SRC

On peut calculer les valeurs moyennes à l'année par un
rééchantillonnage, ~'AS'~ correspondant à [[https://pandas.pydata.org/pandas-docs/stable/user_guide/timeseries.html#offset-aliases][une fréquence annuelle
associée au début de chaque année]] :



#+BEGIN_SRC ipython
ops_FR_yearly = ops_FR[data_columns].resample('AS').mean().iloc[:-1]
ops_FR_yearly
#+END_SRC
#+BEGIN_SRC ipython
ops_FR_monthly_mean=ops_FR[data_columns].resample('M').mean()
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_yearly['wind+solar/load_actual%'] = 100*(ops_FR_yearly['wind'] + ops_FR_yearly['solar'])/ ops_FR_yearly['load_actual']
ops_FR_yearly = ops_FR_yearly.set_index(ops_FR_yearly.index.year)
ops_FR_yearly.index.name = 'year'
ops_FR_yearly
#+END_SRC


#+BEGIN_SRC ipython
sns.barplot(y=ops_FR_yearly['wind+solar/load_actual%'], x=ops_FR_yearly.index );
#+END_SRC


On peut calculer une tendance long terme avec une moyenne sur une
fenêtre glissante de un an :

#+BEGIN_SRC ipython
ops_FR_yearly_trend = ops_FR[data_columns].rolling(window=365*24, center=True).mean()
#+END_SRC

#+BEGIN_SRC ipython
ops_FR['solar'].plot()
ops_FR_yearly_trend['solar'].plot()
plt.legend(['horaire', 'tendance annuelle']);
#+END_SRC


#+BEGIN_SRC ipython
ops_FR['load_actual'].plot()
ops_FR_yearly_trend['load_actual'].plot()
plt.legend(['horaire', 'tendance annuelle'])
#+END_SRC


#+BEGIN_SRC ipython
from statsmodels.tsa.seasonal import seasonal_decompose
decomposition = seasonal_decompose(ops_FR['load_actual'], model='additive',freq=int(24*365))
fig = decomposition.plot()
plt.show()
#+END_SRC


#+BEGIN_SRC ipython
decomposition = seasonal_decompose(ops_FR_monthly_mean['solar'], model='additive',freq=12)
decomposition.plot()
#+END_SRC

#+BEGIN_SRC ipython
train=ops_FR_monthly_mean[:'2017']['load_actual']
test=ops_FR_monthly_mean['2018':]['load_actual']
train.isnull().sum()
#+END_SRC

#+BEGIN_SRC ipython
import numpy as np
from statsmodels.tsa.holtwinters import ExponentialSmoothing
fig, ax = plt.subplots(figsize=(18, 6))
ax.plot(train['2016':].index, train['2016':].values);
ax.plot(test.index, test.values, label='vérité terrain');

for trend in ["add", "multiplicative"]:
    for seasonal in ["add", "multiplicative"]:
        model = ExponentialSmoothing(train,  initialization_method='heuristic',trend= trend, seasonal= seasonal, seasonal_periods=12)
        fit = model.fit()
        pred = fit.forecast(test.size)
        sse = np.sqrt(np.mean(np.square(test.values - pred.values)))
        ax.plot(test.index, pred, linestyle='--', label="tendance :{}, saisonalité : {} (RMSE={:0.2f}, AIC={:0.2f})".format(trend, seasonal, sse, fit.aic));
ax.legend();
ax.set_title("Holt-Winter");
#+END_SRC


* Prophet

Ce qu'on voudrait, c'est pourvoir modéliser à la fois :

- une tendance long terme, mais éventuellement avec des ruptures
- plusieurs tendances cycliques
- des évènements particuliers

On peut utiliser pour cela des [[https://fr.wikipedia.org/wiki/Mod%C3%A8le_additif_g%C3%A9n%C3%A9ralis%C3%A9][modèles additifs géneralisés]]
(Generalized Additive Model) et la [[https://peerj.com/preprints/3190.pdf][bibliothèque prophet]], [[https://research.fb.com/prophet-forecasting-at-scale/][publiée par
Facebook]], [[https://medium.com/future-vision/the-math-of-prophet-46864fa9c55a][implémente ce type de modèle]].


#+BEGIN_SRC ipython
split_date = '2017-12-31'
ops_FR_train = pd.DataFrame(ops_FR.loc[ops_FR.index <= split_date]['load_actual'])
ops_FR_test = pd.DataFrame(ops_FR.loc[ops_FR.index > split_date]['load_actual'])
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_test
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_test \
    .rename(columns={'load_actual': 'test set'}) \
    .join(ops_FR_train.rename(columns={'load_actual': 'training set'}), how='outer') \
    .plot(figsize=(15,5), title='consommation électrique', style='.');
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_train.reset_index().rename(columns={'utc_timestamp':'ds','load_actual':'y'}).head()
#+END_SRC


Contrairement à d'autres modèles de prévision, [[https://github.com/facebook/prophet/issues/627][prophet n'a pas besoin
que les données aient été préalablement normalisées]].

#+BEGIN_SRC ipython
from fbprophet import Prophet
ops_FR_train.index=ops_FR_train.index.tz_localize(None)
model = Prophet()
model.fit(ops_FR_train.reset_index().rename(columns={'utc_timestamp':'ds','load_actual':'y'}));
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_test.index=ops_FR_test.index.tz_localize(None)
ops_FR_test_fcst = model.predict(df=ops_FR_test.reset_index().rename(columns={'utc_timestamp':'ds'}))
#+END_SRC

#+BEGIN_SRC ipython
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
fig = model.plot(ops_FR_test_fcst, ax=ax)
plt.show()
#+END_SRC


#+BEGIN_SRC ipython
fig = model.plot_components(ops_FR_test_fcst)
#+END_SRC

#+BEGIN_SRC ipython
import datetime
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'], color='r', s=5)
fig = model.plot(ops_FR_test_fcst, ax=ax)
ax.set_xbound([datetime.date(2017, 12, 1), ops_FR_test.index.max()])
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_test.index
#+END_SRC

#+BEGIN_SRC ipython
import datetime
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'],s=8, color='r')
fig = model.plot(ops_FR_test_fcst, ax=ax)
ax.set_xbound([datetime.date(2018, 1, 1), datetime.date(2018, 1, 8)])
ax.set_ybound((40000,90000))
plot = plt.suptitle('Janvier 2018 : prévisions et réalisations')
#+END_SRC

#+BEGIN_SRC ipython
from sklearn.metrics import mean_squared_error, mean_absolute_error
mean_squared_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
mean_absolute_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst['yhat'])
#+END_SRC

#+BEGIN_SRC ipython
import pandas.tseries.holiday
help(pandas.tseries.holiday)
#+END_SRC

Pour avoir les vacances Françaises, on peut :
- utiliser une bibliothèque dédiée workalendar.
- [[https://riptutorial.com/fr/pandas/example/25798/calendriers-de-vacances][définir à la main les vacances considérées]]

Pour pouvoir calculer les dates de certaines vacances religieuses, il
faut disposer de bibliothèques d'astronomie (par exemple [[https://github.com/brandon-rhodes/python-sgp4][sgp4]] et
[[https://github.com/conda-forge/skyfield-feedstock][skyfield]]) !


#+BEGIN_SRC ipython
from workalendar.europe import France
calendar = France()
#+END_SRC


#+BEGIN_SRC ipython
holidays=[]
for y in ops_FR['year'].unique():
    holidays= holidays + calendar.holidays(y)
holidays= pd.DataFrame(holidays).rename(columns={0:'ds',1:'holiday'})
holidays
#+END_SRC

#+BEGIN_SRC ipython
model_with_holidays=Prophet(holidays=holidays)
model_with_holidays.fit(ops_FR_train.reset_index().rename(columns={'utc_timestamp':'ds','load_actual':'y'}))
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_test_fcst_holidays = model_with_holidays.predict(df=ops_FR_test.reset_index().rename(columns={'utc_timestamp':'ds'}))
#+END_SRC

#+BEGIN_SRC ipython
mean_squared_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
mean_squared_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst_holidays['yhat'])
#+END_SRC

#+BEGIN_SRC ipython
mean_absolute_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
mean_absolute_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst_holidays['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
import warnings
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'],s=8, color='r')
ax.scatter(ops_FR_test.index, ops_FR_test_fcst['yhat'],s=8, color='g')

fig = model_with_holidays.plot(ops_FR_test_fcst_holidays, ax=ax)
ax.set_xbound([datetime.date(2018, 1, 1), datetime.date(2018, 1, 8)])
ax.set_ybound((40000,90000))
plot = plt.suptitle('Janvier 2018 : prévisions avec vacances et réalisations')
with warnings.catch_warnings(record=True):
    ax.legend(['_nolegend_', 'prévision avec vacances', 'réalisation', 'prévision sans vacances'])
#+END_SRC


#+BEGIN_SRC ipython
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'],s=8, color='r')
ax.scatter(ops_FR_test.index, ops_FR_test_fcst['yhat'],s=8, color='g')

fig = model_with_holidays.plot(ops_FR_test_fcst_holidays, ax=ax)
ax.set_xbound([datetime.date(2018, 4, 30), datetime.date(2018, 5, 5)])
ax.set_ybound((30000,70000))
plot = plt.suptitle('Premier mai 2018 : prévisions avec vacances et réalisations')
with warnings.catch_warnings(record=True):
    ax.legend(['_nolegend_', 'prévision avec vacances', 'réalisation', 'prévision sans vacances'])
#+END_SRC


#+BEGIN_SRC ipython
holidays_list=[]
for i in range(holidays.index.size):
    holidays_list+=pd.date_range(holidays.iloc[i]['ds'], periods=24, freq='H')
holidays_list
#+END_SRC

#+BEGIN_SRC ipython
holidays_test = ops_FR_test.query('index in @holidays_list')
holidays_pred = ops_FR_test_fcst.query('ds in @holidays_list')
holidays_pred_holidays_model = ops_FR_test_fcst_holidays.query('ds in @holidays_list')
#+END_SRC


#+BEGIN_SRC ipython
mean_absolute_error(y_true=holidays_test['load_actual'], y_pred=holidays_pred['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
mean_absolute_error(y_true=holidays_test['load_actual'], y_pred=holidays_pred_holidays_model['yhat'])
#+END_SRC

*Exercices :*

- Comparer avec l'utilisation de
  ~model.add_country_holidays(country_name='France')~
- Observer l'influence du paramètre [[https://facebook.github.io/prophet/docs/seasonality,_holiday_effects,_and_regressors.html#prior-scale-for-holidays-and-seasonality][holidays.prior.scale]].


* XGBoost

#+BEGIN_SRC ipython
import xgboost as xgb
from xgboost import plot_importance, plot_tree
#+END_SRC

#+BEGIN_SRC ipython
ops_FR.columns
#+END_SRC


#+BEGIN_SRC ipython
features=['hour','dayofweek','quarter','month','year',
           'dayofyear','dayofmonth','weekofyear']
#+END_SRC


#+BEGIN_SRC ipython
from sklearn.preprocessing import StandardScaler
std = StandardScaler()
scaled = std.fit_transform(ops_FR[features])
scaled = pd.DataFrame(scaled,columns=features)
scaled.index=ops_FR.index.copy()
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_ml=scaled.merge(ops_FR['load_actual'],left_index=True,right_index=True)
ops_FR_ml
#+END_SRC


#+BEGIN_SRC ipython
ops_FR_ml.loc[ops_FR_ml.index.isin(holidays_list),'holidays']=1.
ops_FR_ml['holidays']= ops_FR_ml['holidays'].fillna(0.)
#ops_FR_ml.loc[['2012-01-01 09:00:00+01:00','2012-01-01 10:00:00+01:00']]
#+END_SRC


#+BEGIN_SRC ipython
X = ops_FR_ml.drop('load_actual',1)
y = ops_FR_ml['load_actual']
X
#+END_SRC


#+BEGIN_SRC ipython
split_date = '2017-12-31'
ops_FR_ml_train=ops_FR_ml.loc[ops_FR_ml.index <= split_date]
ops_FR_ml_test=ops_FR_ml.loc[ops_FR_ml.index > split_date]
#+END_SRC

#+BEGIN_SRC ipython
ops_FR_ml_train
#+END_SRC


#+BEGIN_SRC ipython
xgb_reg = xgb.XGBRegressor(objective ='reg:squarederror', colsample_bytree = 0.3, learning_rate = 0.1, max_depth = 5, alpha = 10, n_estimators = 100)

xgb_reg.fit(ops_FR_ml_train.drop('load_actual',1), ops_FR_ml_train['load_actual'])
#+END_SRC

(On peut ignorer l'[[https://github.com/dmlc/xgboost/issues/4300][avertissement qui apparaît avec les versions de
XGBoost anerieures à 1.0.0]].)

#+BEGIN_SRC ipython
test_pred= xgb_reg.predict(ops_FR_ml_test.drop('load_actual',1))
mean_squared_error(y_true=ops_FR_test['load_actual'], y_pred=test_pred)
#+END_SRC

#+BEGIN_SRC ipython
from sklearn.model_selection import TimeSeriesSplit
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GridSearchCV,RandomizedSearchCV
#+END_SRC


#+BEGIN_SRC ipython
n_iter = 1000
n_splits = 5
random_state = 42 # ou n'importe quoi, mais fixé pour que les résultats soient reproductibles

#xgb_reg = xgb.XGBRegressor(objective ='reg:squarederror', colsample_bytree = 0.3, learning_rate = 0.1,max_depth = 5, alpha = 10, n_estimators = 100)

param_grid = {
        'silent': [False],
        'alpha': [10],
        'objective': ['reg:squarederror'],
        'max_depth': [3, 4, 5, 6],
        'learning_rate': [0.001, 0.01, 0.1, 0.2, 0.3],
        'subsample': [0.8, 0.9, 1.0],
        'colsample_bytree': [0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'colsample_bylevel': [0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0],
        'min_child_weight': [0.5, 1.0, 3.0, 5.0, 7.0, 10.0],
        'gamma': [0, 0.25, 0.5, 1.0],
        'reg_lambda': [0.1, 1.0, 5.0, 10.0, 50.0, 100.0],
        'n_estimators': [100]}

xgb_search = RandomizedSearchCV(xgb.XGBRegressor(objective ='reg:squarederror',
                                                 colsample_bytree = 0.3, learning_rate = 0.1,
                                                 max_depth = 5, alpha = 10, n_estimators = 100),
                                param_grid, n_iter=n_iter,
                            n_jobs=-1, verbose=2, cv=TimeSeriesSplit(n_splits=5),
                            scoring='r2', refit=True, random_state=random_state)
xgb_search.fit(ops_FR_ml_train.drop('load_actual',1), ops_FR_ml_train['load_actual'])
#+END_SRC


#+BEGIN_SRC ipython
test_pred= xgb_search.predict(ops_FR_ml_test.drop('load_actual',1))
mean_squared_error(y_true=ops_FR_test['load_actual'], y_pred=test_pred)
#+END_SRC

#+BEGIN_SRC ipython
mean_squared_error(y_true=ops_FR_test['load_actual'],
                   y_pred=ops_FR_test_fcst_holidays['yhat'])
#+END_SRC


#+BEGIN_SRC ipython
mean_absolute_error(y_true=ops_FR_test['load_actual'], y_pred=test_pred)
#+END_SRC


#+BEGIN_SRC ipython
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'],s=8, color='r')
#ax.scatter(ops_FR_test.index, ops_FR_test_fcst['yhat'],s=8, color='g')
ax.scatter(ops_FR_test.index, test_pred,s=28, color='y')

fig = model_with_holidays.plot(ops_FR_test_fcst_holidays, ax=ax)
ax.set_xbound([datetime.date(2018, 4, 30), datetime.date(2018, 5, 5)])
ax.set_ybound((30000,70000))
plot = plt.suptitle('Premier mai 2018 : prévisions avec vacances et réalisations')
with warnings.catch_warnings(record=True):
    ax.legend(['_nolegend_', 'Prophet: prévision avec vacances', 'réalisation', 'XGBoost : prévision avec vacances'])
#+END_SRC


#+BEGIN_SRC ipython
xgb_search
#+END_SRC


#+BEGIN_SRC ipython
xgb_search.estimator
#+END_SRC


#+BEGIN_SRC ipython
xgb_reg
#+END_SRC

#+BEGIN_SRC ipython
f, ax = plt.subplots(1)
f.set_figheight(5)
f.set_figwidth(15)
ax.scatter(ops_FR_test.index, ops_FR_test['load_actual'], color='r', s=2)
ax.scatter(ops_FR_test.index, ops_FR_test_fcst_holidays['yhat'], color='b', s=2)

ax.scatter(ops_FR_test.index, xgb_search.predict(ops_FR_ml_test.drop('load_actual',1)), color='g', s=2)

ax.set_xbound([datetime.date(2017, 12, 1), ops_FR_test.index.max()])
f.legend(['réalisé','prévision Prophet','prévision XGBoost']);
#+END_SRC


#+BEGIN_SRC ipython
from sklearn.metrics import mean_squared_error
print("RMSE de prophet: %f" % mean_squared_error(ops_FR_test['load_actual'],ops_FR_test_fcst_holidays['yhat'] ))
print("RMSE de XGBoost: %f" % mean_squared_error(ops_FR_test['load_actual'],xgb_search.predict(ops_FR_ml_test.drop('load_actual',1)) ))
#+END_SRC


On peut s'essayer à analyser [[https://www.eia.gov/dnav/pet/hist_xls/RBRTEd.xls][toutes sortes de séries temporelles]] mais
les prix ne sont pas propices aux prévisions aux échelles de temps où
les marchés sont efficients.
