#+TITLE: Paradoxes statistiques, régression vers la moyenne : Travaux Pratiques (fr-FR)
#+AUTHOR: Bernard Hugueney
#+DATE: <2021-06-29 Tue 16:30>
#+LANGUAGE:  fr

#+BEGIN_SRC emacs-lisp :exports none :results silent
(setq ob-ipython-show-mime-types nil)
#+END_SRC


* Paradoxe de Simpson

On essaiera de déterminer si l'Université de Berkeley était injuste envers les candidates en 1973.

Calculer le taux d'acceptation pour les hommes et pour les femmes à
partir du jeu de données du fichier
=./Data/Simpson-Paradox/UC-Berkeley-applicants-no-dept.csv=.

#+BEGIN_SRC ipython
#%load hints/exo_paradox_1-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_1-1.py
#%load solutions/exo_paradox_1.py
import pandas as pd
berkeley= pd.read_csv("./Data/Simpson-Paradox/UC-Berkeley-applicants-no-dept.csv")
help (berkeley.loc)
help (berkeley.groupby) # and count()
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_1.py
import pandas as pd
berkeley= pd.read_csv("./Data/Simpson-Paradox/UC-Berkeley-applicants-no-dept.csv")
groups= berkeley.groupby(['Gender','Admit']).count()
w_acc=groups.loc['Female','Admitted']/berkeley.groupby('Gender').count().loc['Female']['Id']
m_acc=groups.loc['Male','Admitted']/berkeley.groupby('Gender').count().loc['Male']['Id']
(w_acc,m_acc)
#+END_SRC



Ces taux sont-ils /significativement/ différents ?

#+BEGIN_SRC ipython
#%load hints/exo_paradox_2-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_2-1.py
#%load solutions/exo_paradox_2.py
from scipy import stats
help(pd.crosstab)
help(stats.fisher_exact)

#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_2.py
from scipy import stats
_, p_value=stats.fisher_exact(pd.crosstab(berkeley['Admit'], berkeley['Gender']))
p_value*100
#+END_SRC


Afin d'avoir une idée plus précise de la situation, on va s'intéresser
aux taux d'acceptation de chaque département de l'Université en
utilisant le jeu de données du fichier
=./Data/Simpson-Paradox/UC-Berkeley-applicants-dept.csv=.

#+BEGIN_SRC ipython
#%load hints/exo_paradox_3-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_3-1.py
#%load solutions/exo_paradox_3.py
berkeley= pd.read_csv("./Data/Simpson-Paradox/UC-Berkeley-applicants-dept.csv")
tmp=berkeley.groupby(['Dept','Gender','Admit']).count().reset_index(2)
help(tmp.pivot)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_3.py
berkeley= pd.read_csv("./Data/Simpson-Paradox/UC-Berkeley-applicants-dept.csv")
tmp=berkeley.groupby(['Dept','Gender','Admit']).count().reset_index(2).pivot(columns='Admit')
tmp.iloc[:,0]*100/(tmp.iloc[:,0]+tmp.iloc[:,1])
#+END_SRC


Est-ce ce à quoi vous vous attendiez ? Que c'est-il passé ?


*** Un exemple caricatural pour illustrer

Imaginons une Université avec 2 départements :

- Le département A admet 10% des candidats hommes et 15% des candidates femmes.
- La département B admet 45% des candidats hommes et 56% des candidates femmes.

Calculer les taux d'admission au niveau global de l'Université si :
- 50 hommes et 100 femmes candidatent pour le département A
- 100 hommes et 50 femmes candidatent pour le département B

On a 5 hommes admis dans le département A et 45 hommes admis dans le
département B, donc 50 admis sur 150 candidats : 33.3% de taux
d'admission global des hommes.

On a 15 femmes admises dans le département A et 28 femmes admises dans
le département B, donc 43 admises sur 150 candidates : 28.6% de taux
d'admission global des femmes.

** Bonus

En utilisant le jeu de données du fichier
=./Data/Simpson-Paradox/Florida-death-penalty.csv=, essayer de
déterminer si les condamnation à la peine de mort dépendaient des
accusés. en Floride en 1976-1977 (période couverte par le jeu de
données). Si c'était le cas, pouvez-vous trouver une explication ?

#+BEGIN_SRC ipython
#%load hints/exo_paradox_4-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_4-1.py
#%load solutions/exo_paradox_4.py
florida= pd.read_csv("./Data/Simpson-Paradox/Florida-death-penalty.csv")
tmp=florida.groupby(['Defendent','Death']).count().reset_index(1).pivot(columns=['Death'])
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_4.py
florida= pd.read_csv("./Data/Simpson-Paradox/Florida-death-penalty.csv")
tmp=florida.groupby(['Defendent','Death']).count().reset_index(1).pivot(columns=['Death'])
(tmp.iloc[:,1]*100/(tmp.iloc[:,0]+tmp.iloc[:,1])).rename("% death penalty")
#+END_SRC


Maintenant, utiliser le jeu de données du fichier
=./Data/Simpson-Paradox/Florida-death-penalty-victim.csv=.

#+BEGIN_SRC ipython
#%load hints/exo_paradox_5-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_5-1.py
#%load solutions/exo_paradox_5.py
florida= pd.read_csv("./Data/Simpson-Paradox/Florida-death-penalty-victim.csv")
tmp=florida.groupby(['Victim','Defendent','Death']).count().reset_index(2).pivot(columns='Death')
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_5.py
florida= pd.read_csv("./Data/Simpson-Paradox/Florida-death-penalty-victim.csv")
tmp=florida.groupby(['Victim','Defendent','Death']).count().reset_index(2).pivot(columns='Death')
(tmp.iloc[:,1]*100/(tmp.iloc[:,0]+tmp.iloc[:,1])).rename("% death penalty")
#+END_SRC

Que c'est-il passé ? Avez-vous une explication ?

* Paradoxe de Berkson

Nous allons essayer de déterminer si la taille est utile pour marquer
des points au basketball.

En utilisant le jeu de données du fichier
=./Data/Berkson-Paradox/players_stats.csv=
(cf. [[https://www.kaggle.com/drgilermo/nba-players-stats-20142015]]),
calculer la corrélation entre le nombre de points marqués (colonne
~PTS~) et la taille des joueurs (colonne ~Height~).


#+BEGIN_SRC ipython
#%load hints/exo_paradox_6-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_6-1.py
#%load solutions/exo_paradox_6.py
import seaborn as sns
from scipy.stats.stats import pearsonr
basketball= pd.read_csv("../../data/Berkson-Paradox/players_stats.csv")
help(basketball.dropna)
help(sns.jointplot)
help(pearsonr)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_6.py
import seaborn as sns
from scipy.stats.stats import pearsonr
basketball= pd.read_csv("../../data/Berkson-Paradox/players_stats.csv")
basketball=basketball.loc[:,['Height','PTS']].dropna()
g=sns.jointplot(x='Height', y='PTS', data= basketball, kind="reg")
r,p_value=pearsonr(basketball['PTS'],basketball['Height'])
g.fig.suptitle(f"pearson R={r:.4E}, p value={p_value:.4E}");
#+END_SRC


Que pensez-vous de ce résultat ? Que c'est-il passé ?
Pouvez-vous imaginer d'autres situations similaires ?

* Régression vers la moyenne

Nous allons essayer de déterminer si la taille de la population change
d'une générationà l'autre, en s'intéressant aux extrêmes :
- est-ce que les familles les plus grandes (taille des individus) restent aussi grandes ?
- est-ce que les familles les plus petites (taille des individus) restent aussi petites ?

** Data engineering

Après avoir chargé le jeu de données du fichier
=./Data/Heights/Galton.txt= :

1. vérifier s'il y ades valeurs manquantes / aberrantes et les
   corriger le cas échéant.

#+BEGIN_SRC ipython
#%load hints/exo_paradox_7-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_7-1.py
#%load solutions/exo_paradox_7.py
help(pd.read_csv)
help(pd.DataFrame().isnull)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_7.py
galton= pd.read_csv("./Data/Heights/Galton.txt", header=0, sep="\t")
print(galton.isnull().sum())
print((galton==0).sum())
#+END_SRC

2. convertir les tailles de pouces en centimètres

#+BEGIN_SRC ipython
#%load hints/exo_paradox_8-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_8-1.py
#%load solutions/exo_paradox_8.py
help(pd.DataFrame().loc)
pd.DataFrame().loc[:,]*2.54
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_8.py
galton.loc[:,['Height','Father','Mother']]=galton.loc[:,['Height','Father','Mother']]*2.54
#+END_SRC

3. Créer une nouvelle variable ~Parents~ qui est la moyenne des taille
   du père et de la mère.

#+BEGIN_SRC ipython
#%load hints/exo_paradox_9-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_9-1.py
#%load solutions/exo_paradox_9.py
galton["Father"]+galton["Mother"]
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_9.py
galton["Parents"]=(galton["Father"]+galton["Mother"])/2
#+END_SRC

** Moyenne

1. Sélectionner les familles pour lesquelles ~Parents~ est dans le
   quartile supérieur. Quelles sont les moyennes de tailles des
   parents et des enfants dans ce quartile ?

#+BEGIN_SRC ipython
#%load hints/exo_paradox_10-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_10-1.py
#%load solutions/exo_paradox_10.py
galton["Father"]+galton["Mother"]
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_10.py
galton["ParentsCat"]="Average"
galton.loc[galton["Parents"]<galton["Parents"].quantile(0.25),  "ParentsCat"]="Short"
galton.loc[galton["Parents"]>galton["Parents"].quantile(0.75),"ParentsCat"]="Tall"
(galton.loc[galton["ParentsCat"]=='Tall', ['Family','Parents']].groupby('Family').mean().mean()
,galton.loc[galton["ParentsCat"]=='Tall', 'Height'].mean())
#+END_SRC

Ces moyennes sont-elles /significativement/ dufférentes ?

#+BEGIN_SRC ipython
#%load hints/exo_paradox_11-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_11-1.py
#%load solutions/exo_paradox_11.py
import statsmodels.stats.api as sms
help(sms.DescrStatsW)
help(sms.DescrStatsW([]).tconfint_mean)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_11.py
import statsmodels.stats.api as sms
(sms.DescrStatsW(galton.loc[galton["ParentsCat"]=='Tall', ['Family','Parents']].groupby('Family').mean()).tconfint_mean(alpha=0.1)
,sms.DescrStatsW(galton.loc[galton["ParentsCat"]=='Tall','Height']).tconfint_mean(alpha=0.1))
#+END_SRC



2. Sélectionner les familles pour lesquelles ~Average~ est dans le
   quartile inférieur. Quelles sont les moyennes des tailles des
   parents et des enfants de ce quartile ?

#+BEGIN_SRC ipython
#%load hints/exo_paradox_12-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_12-1.py
#%load solutions/exo_paradox_12.py
galton["Father"]+galton["Mother"]
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_12.py
(galton.loc[galton["ParentsCat"]=='Short', ['Family','Parents']].groupby('Family').mean().mean()
,galton.loc[galton["ParentsCat"]=='Short', 'Height'].mean())
#+END_SRC

Ces moyennes sont-elles /significativement/ différentes ?

#+BEGIN_SRC ipython
#%load hints/exo_paradox_13-1.py
#TODO
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./hints/exo_paradox_13-1.py
#%load solutions/exo_paradox_13.py
import statsmodels.stats.api as sms
help(sms.DescrStatsW)
help(sms.DescrStatsW([]).tconfint_mean)
#+END_SRC

#+BEGIN_SRC ipython :mkdirp yes :exports none :tangle ./solutions/exo_paradox_13.py
import statsmodels.stats.api as sms
(sms.DescrStatsW(galton.loc[galton["ParentsCat"]=='Short', ['Family','Parents']].groupby('Family').mean()).tconfint_mean(alpha=0.05)
,sms.DescrStatsW(galton.loc[galton["ParentsCat"]=='Short','Height']).tconfint_mean(alpha=0.05))
#+END_SRC


Que c'est-il passé ?
Pouvez-vous penser à d'autres situations similaires ?




